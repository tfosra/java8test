/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java8.StringCollector;
import java8.StringCombiner;
import java8.entity.City;
import java8.entity.Country;
import java8.entity.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author s.rapaya
 */
public class MainTest {

    static List<Country> countries = new ArrayList<Country>() {
        {
            add(new Country("Cameroon"));
            add(new Country("Gabon"));
            add(new Country("Japan"));
            add(new Country("Spain"));
        }
    };

    static List<City> cities = new ArrayList<City>() {
        {
            add(new City("Yaounde", countries.get(0)));
            add(new City("Garoua", countries.get(0)));
            add(new City("Batouri", countries.get(0)));
            add(new City("Douala", countries.get(0)));
            add(new City("Libreville", countries.get(1)));
            add(new City("Tokyo", countries.get(2)));
            add(new City("Seoul", countries.get(2)));
            add(new City("Nagazaki", countries.get(2)));
            add(new City("Madrid", countries.get(3)));
            add(new City("Barcelona", countries.get(3)));
        }
    };

    static List<User> users = new ArrayList<User>() {
        {
            add(new User("John Doe", 2, cities.get(0),
                    cities.get(1),
                    cities.get(5)));
            add(new User("Souleyman", 5, cities.get(7),
                    cities.get(5),
                    cities.get(2),
                    cities.get(9)));
            add(new User("Rapaya", 1, cities.get(3),
                    cities.get(4),
                    cities.get(6),
                    cities.get(8),
                    cities.get(0)));
            add(new User("Titeuf", 8, cities.get(2)));
        }
    };

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    public static Optional<User> findUser(List<User> users, String name) {
        return users.stream()
                .filter(user -> user.getName().contains(name))
                .findFirst();
    }

    @Test
    public void reduceExample() {
        users.stream()
                .filter(user -> user.getCities().stream().anyMatch(city -> city.getCountry().getName().equals("Japan")))
                .map(User::getLevel)
                .reduce(0, (acc, level) -> acc + level);
    }

    @Test
    public void mapExample1() {
        cities.stream()
                .map(City::getName)
                .forEach(System.out::println);
    }

    @Test
    public void mapExample2() {
        cities.stream()
                .reduce(new ArrayList<String>(), (acc, user) -> {
                    acc.add(user.getName());
                    return acc;
                }, (acc1, acc2) -> acc2)
                .forEach(System.out::println);
    }

    @Test
    public void statisticExample() {
        IntSummaryStatistics summaryStatistics = users.stream()
                .mapToInt(User::getLevel)
                .summaryStatistics();

        System.out.println(summaryStatistics.getMin());
    }

    @Test
    public void optionalExample() {
        Optional<User> user = findUser(users, "xdj");
        System.out.println(user.orElse(null));
    }

    @Test
    public void groupExample1() {
        Map<Integer, List<String>> collect = users.stream()
                .collect(Collectors.groupingBy(User::getLevel, Collectors.mapping(User::getName, Collectors.toList())));
        System.out.println(collect);
    }

    @Test
    public void testJoining1() {
        String collect = users.stream()
                .map(User::getName)
                .collect(Collectors.joining(", ", "{", "}"));
        System.out.println(collect);
    }

    @Test
    public void testJoining2() {
        String combined = users.stream()
                .map(User::getName)
                .reduce(new StringCombiner(", ", "<", ">"),
                        StringCombiner::add,
                        StringCombiner::merge)
                .toString();
        System.out.println(combined);
    }

    @Test
    public void testJoining3() {
        String collect = users.stream()
                .map(User::getName)
                .collect(new StringCollector(", ", "<", ">"));
        System.out.println(collect);
    }

    @Test
    public void testForEach() {
        users.stream()
                .collect(Collectors.groupingBy(User::getLevel, Collectors.mapping(User::getName, Collectors.toList())))
                .forEach((key, value) -> System.out.println(key + "#" + value));
    }

    @Test
    public void testReduce2() {
        Optional<String> max = Stream.of("John Lennon", "Paul McCartney",
                "George Harrison", "Ringo Starr", "Pete Best", "Stuart Sutcliffe")
                .max(String::compareTo);
        System.out.println(max);

        Map<String, Long> collect = Stream.of("John", "Paul", "George", "John", "Paul", "John")
                .collect(Collectors.groupingBy(str -> str, Collectors.counting()));
        System.out.println(collect);
    }
}
