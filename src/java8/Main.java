/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8;

import java.util.stream.IntStream;

/**
 *
 * @author s.rapaya
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int upTo = 4000;
        long count = IntStream.range(1, upTo)
                .parallel()
                .filter(Main::isPrime)
                .count();
        System.out.println("There are " + count + " primes up to " + upTo);
    }
    
    public static boolean isPrime(int nbr) {
        return IntStream.range(2, nbr)
                .parallel()
                .allMatch(x -> (nbr % x) != 0);
    }

}
