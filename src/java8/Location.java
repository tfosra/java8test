/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8;

/**
 *
 * @author s.rapaya
 */
public interface Location {
    
    default String sayHello(String name) {
        return "Hello " + name;
    }
    
    static String getCurrentNamer() {
        return "TOTO";
    }
    
}
