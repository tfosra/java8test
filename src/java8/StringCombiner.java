/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8;

/**
 *
 * @author s.rapaya
 */
public class StringCombiner {
    
    private final String prefix;
    private final String suffix;
    private final String delimiter;
    
    private final StringBuilder builder;

    public StringCombiner(String delimiter, String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.delimiter = delimiter;
        builder = new StringBuilder();
    }

    public StringCombiner add(String str) {
        if (builder.length() == 0) {
            builder.append(prefix);
        }
        else {
            builder.append(delimiter);
        }
        builder.append(str);
        return this;
    }
    
    public StringCombiner merge(StringCombiner other) {
        builder.append(other.builder);
        return this;
    }

    @Override
    public String toString() {
        return builder.toString() + suffix;
    }
    
}
