/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author s.rapaya
 */
public class User {

    private String name;
    private int level;
    private List<City> cities;

    public User(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public User(String name, int level, City... cities) {
        this.name = name;
        this.level = level;
        this.cities = new ArrayList<>(Arrays.asList(cities));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + '}';
    }
}
